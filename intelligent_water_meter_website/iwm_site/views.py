from django.shortcuts import render,redirect,render_to_response
from . import models
from .models import *
from django.contrib.auth import authenticate,login,logout,update_session_auth_hash
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.forms import SetPasswordForm
from django.contrib import messages
from django.http import HttpResponse
from .forms import EditForm
from django.utils.safestring import mark_safe
import json


# Create your views here.
def index(request):
	# if request.method == "POST":
	# 	post = request.POST
	# 	username = post.get('username')
	# 	password = post.get('password')
	# 	possword = make_password(password)
	# 	if not all([username,password]):
	# 		return render(request,'home/home.html',{'errmsg':'please input username and password'})

	# 	user = authenticate(username = username,password = password)
	# 	if user:
	# 		if user.is_active:
	# 			login(request,user)
	# 			# return HttpResponse("login success.")
	# 			return render(request, 'home/home.html')
	# 		else:
	# 			return  HttpResponse("Your account is disabled.")
	# 	else:
	# 		return render(request,'home/home.html',{'errmsg':"Invalid username or password!"})
	return render(request, 'home/home.html')

def contact(request):
	return render(request, 'contact/contact.html')

def signup(request):
	if request.method == "POST":
		post = request.POST
		username = post.get('username')
		password = post.get('password')
		possword = make_password(password)
		if not all([username,password]):
			return render(request,'login/login.html',{'errmsg':'please input username and password'})

		user = authenticate(username = username,password = password)
		if user:
			if user.is_active:
				login(request,user)
				# return HttpResponse("login success.")
				return render(request, 'home/home.html')
			else:
				return  HttpResponse("Your account is disabled.")
		else:
			return render(request, 'home/home.html', {'errmsg':"Invalid username or password!"})
	return render(request,'login/login.html')

def register(request):
	if request.method == "POST":
		post = request.POST
		username = post.get('username')
		exist = User.objects.filter(username = username)
		if (len(exist) == 0):
			userPassword = post.get('userPassword')
			userRePassword = post.get('userRePassword')
			userPhone = post.get('userPhone')
			userEmail = post.get('userEmail')
			useraddress = post.get('useraddress')
			if userPassword != userRePassword:
				return redirect('register/register.html',{'errmsg':"wrong comfire password"})

			User.objects.create_user(username=username,password = userPassword)
			user = 	Customer()
			user.UserName = username
			user.password = make_password(userPassword)
			user.homeAddress = useraddress
			user.phoneNumber = userPhone
			user.emailaddress = userEmail
			user.save()

			return render(request, 'home/home.html')
		else:
			render(request, 'register/register.html',{'errmsg':"Username existed"})
	return render(request, 'register/register.html')


def dashboard(request):
	r = Customer.objects.get(UserName__iexact = request.user.username)
	thisid = Customer.objects.get(UserName = request.user.username)
	water = WaterMeterInfo.objects.filter(user = thisid)
	result = []
	avg = 0;
	count = 0;
	for i in water:
		result.append(i)
		avg += int(i.waterUsage)
		count+=1
	return render(request, 'dashboard/dashboard.html', {'r':r,'result':result , 'avg':avg/count})
def userlogout(request):
	logout(request)
	return render(request, 'logout/logout.html')

def checkout(request):
    return render(request,'checkout/payment.html')

def editprofile(request):
	if request.method == 'POST':
		form = EditForm(request.POST, instance=request.user)

		if form.is_valid():
			user = Customer.objects.get(UserName__iexact = request.user.username)
			user.homeAddress = request.POST['homeAddress']
			user.phoneNumber = request.POST['phoneNumber']
			user.emailaddress = request.POST['emailaddress']
			user.birth = request.POST['birth']
			user.save()

			return redirect('/dashboard')
			
	else:
		form = EditForm(instance=request.user)
		args = {'form':form}
		return render(request, 'dashboard/edit.html', args)

def changepwd(request):
	if request.method == 'POST':
		form = SetPasswordForm(data=request.POST, user=request.user)

		if form.is_valid():
			user = form.save()
			update_session_auth_hash(request, user)
			return redirect('/dashboard')

	else:
		form = SetPasswordForm(user=request.user)
		args = {'form':form}
		return render(request, 'dashboard/pwd.html', args)
def invoice(request):
    r = Customer.objects.get(UserName__iexact = request.user.username)
    thisid = Customer.objects.get(UserName = request.user.username)
    water = WaterMeterInfo.objects.filter(user = thisid)
    result = []
    avg = 0;
    count = 0;
    for i in water:
    	result.append(i)
    	avg+= int(i.waterUsage)
    	count +=1

    return render(request,'invoice/invoice2.html', {'r':r,'result':result , 'avg':avg/count})
def adminDash(request):
	c = Customer.objects.all();
	return render(request, 'adminDash/adminDash.html', {'c' : c})

def chatroom(request):
	c = Message.objects.order_by('-timestamp').all()
	# result = Message.objects.values_list('author',flat = True).distinct()
	result = c.distinct()

	return render(request, 'chatroom/chat.html', {'c':c, 'result':result});

def room(request, room_name):
	return render(request, 'chatroom/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name))
    });