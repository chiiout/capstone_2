from django.contrib import admin
from .models import Bill, Account, WaterMeterInfo, BankAccount, Customer

# enable to display the table
class BillAdmin(admin.ModelAdmin):
	list_display = ('id', 'amount', 'dueDate')

class AccountAdmin(admin.ModelAdmin):
	list_display = ('id','accountBalance')

class WaterMeterInfoAdmin(admin.ModelAdmin):
	list_display = ('id', 'waterUsage', 'previousUsage', 'averageUsage')

class BankAccountAdmin(admin.ModelAdmin):
	list_display = ('id', 'cardNumber', 'holderName', 'expireDate', 'cardType')

class CustomerAdmin(admin.ModelAdmin):
	list_display = ('id', 'UserName', 'password','homeAddress', 'phoneNumber', 'emailaddress','birth', 'bankAccount')
# Register your models here.
admin.site.register(Bill, BillAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(WaterMeterInfo, WaterMeterInfoAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(BankAccount, BankAccountAdmin)