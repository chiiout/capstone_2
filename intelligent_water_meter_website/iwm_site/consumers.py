from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from .models import Message, Customer

class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def message_to_db(self, data):
        author = data['author']
        author_user = Customer.objects.filter(UserName = author)
        message = Message.objects.create(
            author = author,
            content = data['message'])

    def message_from_db(self,data):
        messages = Message.last_30_messages();
        content = {
            'command' : 'msg',
            'messages' : self.messages_to_json(messages)

        }
        print(content['messages'][1])
        self.send_message(content)

    commands = {
        'message_to_db' : message_to_db,
        'message_from_db': message_from_db
    }

    def messages_to_json(self,messages):
        result = []
        for message in messages:
            result.append(self.message_to_json(message))
        return result

    def message_to_json(self,message):
        return {
            "message": message.content,
            "author" : message.author,
            "timestamp": str(message.timestamp),
        }

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        author = text_data_json['author']
        # header = text_data_json['header']
        # footer = text_data_json['footer']
        command = text_data_json['command']
        # # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                # 'header' : header,
                'type': 'send_message',
                'message': message,
                'author' : author,
                # 'footer' : footer,
                'command' : command,
            }
        )
        self.commands[text_data_json['command']](self,text_data_json)
    
    def send_message(self,message):
        self.send(text_data = json.dumps(message))

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']
        author = event['author']
        # header = event['header']
        # footer = event['footer']
        command = event['command']
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            # 'header' : header,
            'message': message,
            'author' : author,
            # 'footer' : footer,
            'command' : command,
        }))