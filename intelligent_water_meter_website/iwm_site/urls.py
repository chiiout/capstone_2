from django.conf.urls import url
from django.urls import path,re_path
from . import views

urlpatterns = [
	url(r'^$', views.index),
	url('contact/', views.contact),
	url(r'register', views.register),
	url(r'signup', views.signup),
    url(r'dashboard', views.dashboard),
    url(r'logout', views.userlogout),
    url(r'checkout', views.checkout),
    url('edit/', views.editprofile),
    url('changepwd/', views.changepwd),
    url(r'logout', views.userlogout),
    url(r'invoice', views.invoice),
    url(r'adminDash', views.adminDash),
    path('chatroom', views.chatroom),
    path('chatroom/<str:room_name>/', views.room, name='room'),
]
