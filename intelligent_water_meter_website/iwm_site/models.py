from django.db import models

# Create your models here.
class Bill (models.Model):
        amount = models.CharField(max_length=100)
        dueDate = models.DateField()

        def __str__(self):
            return "%s due at %s" % (self.amount, self.dueDate)

class Account (models.Model):
	accountBalance = models.CharField(max_length=100)



class BankAccount (models.Model):
	cardNumber = models.CharField(max_length=100)
	holderName = models.CharField(max_length=100)
	expireDate = models.DateField()
	cardType = models.CharField(max_length=50)

class Customer (models.Model):
    UserName = models.CharField(max_length=100,null = True)
    password = models.CharField(max_length=100,null = True)
    homeAddress = models.CharField(max_length=200,null = True)
    phoneNumber = models.CharField(max_length=100,null = True)
    emailaddress = models.EmailField(max_length=100,null = True)
    birth = models.DateField(null = True)
    bankAccount = models.ForeignKey(BankAccount, on_delete = models.CASCADE, null = True)
    # waterUsage = models.OneToOneField(WaterMeterInfo, on_delete = models.CASCADE, null = True)
    balance = models.OneToOneField(Account, on_delete = models.CASCADE, null = True)
    bill = models.OneToOneField(Bill, on_delete = models.CASCADE, null = True)

    def __str__(self):
        # return "%s %s" % (self.firstName, self.lastName)
        return self.UserName
    def get_password(self):
        return self.password
    def get_homeAddress(self):
        return self.homeAddress
    def get_phoneNumber(self):
    	return self.phoneNumber
    def get_emailaddress(self):
    	return self.emailaddress
    def get_birth(self):
    	return self.birth
    def get_bankAccount(self):
    	return self.bankAccount

class WaterMeterInfo (models.Model):
    waterUsage = models.CharField(max_length=200)
    previousUsage = models.CharField(max_length=200, null = True)
    averageUsage = models.CharField(max_length=200, null = True)
    user = models.ForeignKey(Customer, on_delete = models.CASCADE, null = True)

class Message (models.Model):
    # author = models.ForeignKey(Customer, related_name='author_messages', on_delete = models.CASCADE)
    author = models.CharField(max_length=100,null = True)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.author
    def last_30_messages():
        return Message.objects.order_by('-timestamp').all()[:10]
