# Generated by Django 2.1 on 2019-10-22 02:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('iwm_site', '0007_message_author'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='watermeterinfo',
            name='customer',
        ),
        migrations.AddField(
            model_name='customer',
            name='balance',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='iwm_site.Account'),
        ),
        migrations.AddField(
            model_name='customer',
            name='bill',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='iwm_site.Bill'),
        ),
        migrations.AddField(
            model_name='customer',
            name='waterUsage',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='iwm_site.WaterMeterInfo'),
        ),
    ]
