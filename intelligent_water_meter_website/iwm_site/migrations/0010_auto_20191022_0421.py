# Generated by Django 2.1 on 2019-10-22 04:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iwm_site', '0009_auto_20191022_0419'),
    ]

    operations = [
        migrations.AlterField(
            model_name='watermeterinfo',
            name='averageUsage',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='watermeterinfo',
            name='previousUsage',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
