# Generated by Django 2.1 on 2019-10-21 09:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('iwm_site', '0004_auto_20191021_0925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='author_messages', to='iwm_site.Customer'),
        ),
    ]
