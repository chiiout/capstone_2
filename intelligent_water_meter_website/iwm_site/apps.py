from django.apps import AppConfig


class IwmSiteConfig(AppConfig):
    name = 'iwm_site'
