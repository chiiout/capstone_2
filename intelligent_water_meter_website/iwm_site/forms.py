from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from .models import Customer
from django.forms import ModelForm

class EditForm(ModelForm):
    password = None

    class Meta:
        model = Customer
        fields = (
            'emailaddress',
            'homeAddress',            
            'phoneNumber',
            'birth',
        )
